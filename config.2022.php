<?php

$internal_current_date_do_not_read = date('Y-m-d');

date_default_timezone_set("Europe/Rome");

$current_year = '2022';
$computer_date = '2022-10-22';
$shipping_date = '2022-10-07';
$human_date = 'Sabato 22 Ottobre 2022';
$administrators = ['bob@linux.it', 'gnu@linux.it', 'direttore@linux.it'];

$is_virtual  = true;
$is_physical = true;

$sessions = [
	'one' => (object) [
        'label' => 'Sala 1',
        'desc' => '',
        'player' => 'https://garr.tv/s/6351966e1fd3a1a440655c3a?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-22',
    ],
    'two' => (object) [
        'label' => 'Sala 2',
        'desc' => '',
        'player' => 'https://garr.tv/s/635197e53719a67318854ffa?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-22',
    ],
];

$talks_date = '2022-09-30';
$human_talks_date = 'Venerdi 30 Settembre';

// alphabetical order
$sponsors = [
	'Continuity' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/continuity.png',
		'link' => 'https://continuity.space/',
	],
	'Bosh Rexroth' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/bosh_rexroth.png',
		'link' => 'https://www.boschrexroth.com/',
	],
	'Extraordy' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/extraordy.png',
		'link' => 'https://www.extraordy.com/',
	],
	'Linux Professional Institute' => (object) [
		'logo' => 'https://www.ils.org/images/sponsor/lpi.png',
		'link' => 'https://www.lpi.org/it/',
	],
];

$supporters = [
	'GARR' => (object) [
		'logo' => '/immagini/garr.png',
		'link' => 'https://garr.it/',
	],
];

$patronages = [];

$theme = [];
