<?php
/**
 * This is a stupid middleware that emulates Laravel
 * Eloquent query builder, but using a stupid JSON file
 * or whatever array LOL instead of MySQL, since
 * for historical reasons this website worked with JSON.
 * At least now you have a query builder \o/ lol.
 * So in the future the backend may easily change,
 * I guess.
 */
class LinuxDayEventCollection {

	/**
	 * This is the original dataset.
	 * Ideally this array will never be touched twice.
	 * This is the source of truth when you call "query()"
	 * to start a brand-new query.
	 */
	private $originalEvents = [];

	/**
	 * This is a sort of temporary result set,
	 * that will be filtered, sorted, etc.
	 * This is what you obtain by calling "get()".
	 */
	private $events = [];

	/**
	 * Constructor, assuming the default year.
	 */
	public function __construct() {
		// TODO: avoid global variables
		$events_file = $GLOBALS['events_file'] ?? null;
		if (!$events_file) {
			throw new Exception("Missing events file");
		}

		// No file, just do nothing successfully.
		if (!file_exists($events_file)) {
			return;
		}

		// Parse the data.
		$file_content = file_get_contents($events_file);
		if (!$file_content) {
			throw new Exception("Cannot open file {$events_file}");
		}
		$events_raw = json_decode($file_content);
		if ($events_raw === false) {
			throw new Exception("Unable to parse JSON file {$events_file}");
		}

		// Wrap each JSON row in a meaningful class.
		$this->originalEvents = LinuxDayEvent::createFromDataArray($events_raw);
		$this->events = $this->originalEvents;
	}

	/**
	 * Start doing a new query.
	 * @return $this
	 */
	public function query() {
		$this->events = $this->originalEvents;
		return $this;
	}

	public function whereIsApproved() {
		return $this->where( function( $event ) {
			return $event->approvato;
		} );
	}

	public function whereHasEarlyDate() {
		return $this->where( function( $event ) {
			return $event->hasEarlyDate();
		} );
	}

	public function whereHasLateDate() {
		return $this->where( function( $event ) {
			return $event->hasLateDate();
		} );
	}

	public function whereHasTraditionalDate() {
		return $this->where( function ( $event ) {
			return $event->hasTraditionalDate();
		} );
	}

	/**
	 * Order events by province.
	 * @return $this
	 */
	public function orderByProvince() {
		return $this->orderBy( function( $a, $b ) {
			return $a->prov <=> $b->prov;
		} );
	}

	/**
	 * Order events by province.
	 * @return $this
	 */
	public function orderByDate() {
		return $this->orderBy( function( $a, $b ) {
			$a_date = $a->getDateOrDefault();
			$b_date = $a->getDateOrDefault();
			return $a_date <=> $b_date;
		} );
	}

	/**
	 * Order events by date, and province.
	 * @return $this
	 */
	public function orderByDateAndCity() {
		return $this->orderBy( function( $a, $b ) {
			$a_date = $a->getDateOrDefault();
			$b_date = $b->getDateOrDefault();
			return [ $a_date, $a->city ] <=> [ $b_date, $b->city ];
		} );
	}

	/**
	 * Apply the default order.
	 * @return $this
	 */
	public function orderByDefault() {
		return $this->orderByDateAndCity();
	}

	/**
	 * Order by a specific user-provided factor.
	 * @param $callback callable
	 * @return $this
	 */
	private function orderBy($callback) {
		usort($this->events, $callback);
		return $this;
	}

	/**
	 * Apply a specific user-provided condition.
	 * @param $callback callable
	 * @return $this
	 */
	private function where($callback) {
		$this->events = array_filter($this->events, $callback);
		return $this;
	}

	/**
	 * Query the elements.
	 * @return array
	 */
	public function get() {
		return $this->events;
	}

}
