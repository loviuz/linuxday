<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2013';
$computer_date = '2013-10-26';
$shipping_date = '2013-10-26';
$human_date = 'Sabato 26 Ottobre 2013';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
];

$supporters = [];
$patronages = [];

$theme = [];
